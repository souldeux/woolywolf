FROM alpine:edge

RUN apk --no-cache --update upgrade
RUN apk --no-cache --update add \
    python3-dev \
    py3-numpy \
    py3-pillow

RUN pip3 install --upgrade pip setuptools \
    && pip3 install base58 \
    && rm -rf /root/.cache/pip

RUN adduser -D web
USER root

ADD . /opt/woolywolf
WORKDIR /opt
RUN /usr/bin/find . -type d -exec chown web {} \;
WORKDIR /opt/woolywolf
RUN python3 -m compileall -f -q .
USER web
