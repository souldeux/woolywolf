The start of what could be an interesting project, embedding data into images
by treating the RGB values of certain pre-determined pixels as directions in a
base 64 map.
