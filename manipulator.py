from PIL import Image, ImageFile
from io import BytesIO
import numpy as np
import base64, base58, constants, random, json

class ImageEmbedder:
    '''
    Helper class for embedding an embedImage into a hostImage.

    Note: use lossless compression or be very sad.
    '''
    def __init__(self, embed_image, host_image):
        self.embedImage = embed_image
        self.hostImage = host_image
        self.lenChunkList = 0

        self.coords = None


    def _encode_embed_image(self):
        '''
        Take an image file, read it into memory, convert it to a base64
        string, convert each character of that string into its matching base64
        numerical value, and store those values in ordered sets of three
        '''

        #Read the embedImage into a b64 string
        with open(self.embedImage, 'rb') as f:
            _tmp = base64.b64encode(f.read()).decode('utf-8')

        #Convert each character of that b64 string into numerical values
        numList = [int(constants.BASE_64_ENCODER_MAP[char]) for char in _tmp]

        #Chunk the numList into tri-tuples (triples? triuples?)
        chunkList = [ numList[i:i+3] for i in range(0, len(numList), 3) ]

        #If the last chunk in the list is short, pad it out with values greater than 64
        while len(chunkList[-1]) < 3:
            chunkList[-1].append(random.randint(65, 255))

        self.lenChunkList = len(chunkList)
        return chunkList

    def _generate_coordinates(self):
        '''
        Generate mapping for coordinates in the hostImage that need altering
        '''
        random.seed(constants.SEED)
        im = Image.open(self.hostImage)
        self.coords = list()
        while len(self.coords) < self.lenChunkList:
            _c = (random.randint(0, im.size[0]-1), random.randint(0, im.size[1]-1))
            if _c not in self.coords:
                self.coords.append(_c)
#        self.coords = [
#            (random.randint(0, im.size[0]-1), random.randint(0, im.size[1]-1)) for x in range(0, self.lenChunkList)
#        ]
        return self.coords

    def _generate_decode_key(self):
        _ = {
            'seed': constants.SEED,
            'alteredPixelCount': self.lenChunkList
        }
        return base58.b58encode(json.dumps(_))

    def embed_image(self):
        print("Chunking embed image...")
        chunkList = self._encode_embed_image()
        print("Generating coordinates...")
        coordinates = self._generate_coordinates()

        assert len(chunkList) == len(coordinates)

        print("Altering pixels in host...")
        im = Image.open(self.hostImage)
        px = im.load()
        for coord in coordinates:
            chunk = chunkList.pop(0)
            px[coord[0], coord[1]] = (chunk[0], chunk[1], chunk[2])

        print("Writing composite image...")
        im.save('outputs/embedded.png')
        return self._generate_decode_key()


class ImageRipper:

    def __init__(self, image, key):
        self.inputImage = image
        self.outputImage = None

        _ = json.loads(base58.b58decode(key))
        self.seed = _['seed']
        self.alteredPixelCount = _['alteredPixelCount']

        self.coords = None

    def _generate_coordinates(self):
        '''
        Generate mapping for coordinates in the hostImage that need fetching
        '''
        random.seed(self.seed)
        im = Image.open(self.inputImage)
        self.coords = list()
        while len(self.coords) < self.alteredPixelCount:
            _c = (random.randint(0, im.size[0]-1), random.randint(0, im.size[1]-1))
            if _c not in self.coords:
                self.coords.append(_c)

        #self.coords = [
        #    (random.randint(0, im.size[0]-1), random.randint(0, im.size[1]-1)) for x in range(0, self.alteredPixelCount)
        #]
        return self.coords

    def _fetch_values_and_write(self, coordinates):
        result = list()
        im = Image.open(self.inputImage)
        px = im.load()
        for coord in coordinates:
            rgb = px[coord[0], coord[1]]
            for val in rgb:
                if val < 65:
                    result.append(constants.BASE_64_DECODER_MAP[str(val)])

        decoded = base64.b64decode( ''.join(result) )

        with open('outputs/extracted.png', 'wb') as f:
            f.write(decoded)

    def extract(self):
        return self._fetch_values_and_write(self._generate_coordinates())




if __name__ == '__main__':
    ie = ImageEmbedder('assets/toast.png', 'assets/nasa_jpl_cygnus_loop.png')
    key = ie.embed_image()

    ir = ImageRipper('outputs/embedded.png', key)
    ir.extract()
